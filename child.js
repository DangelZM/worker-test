const puppeteer = require('puppeteer');
const uuid = require('uuid');

const browser = puppeteer.launch();
let i = 0;
const name = uuid();

process.on('message', (task) => {
  i++;
  console.log(`Worker ${name} got task`);
  browser.then(async browser => {
    const page = await browser.newPage();
    await page.setViewport({width:1366, height:800});
    await page.goto(task.url);
    await page.screenshot({path: `screens/${task.imageName}`});
    await page.close();
    console.log(`Worker ${name} done ${i} tasks`);
    process.send({
      status: "done",
      imageName: task.imageName
    })
  }).catch(console.log)
});