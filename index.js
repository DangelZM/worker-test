const cp = require('child_process');
const http = require('http');
const uuid = require('uuid');

const MAX_WORKER_COUNT = 20;
let workers = {};
let workersInWaiting = [];

http.createServer((request, response) => {
  taskRunner({
    url: "http://google.com/"
  });
  response.writeHead(200, {'Content-Type': 'application/json'});
  response.end(JSON.stringify({}));
}).listen(3000);


function taskRunner(task){
  task.imageName = `${uuid()}.png`;
  console.time(task.imageName);
  if(Object.keys(workers).length < MAX_WORKER_COUNT) {
    const wid = uuid();
    const worker = cp.fork(`${__dirname}/child.js`);

    workers[wid] = worker;
    worker.on('message', (m) => {
      if(m.status === 'done'){
        console.log(`Screenshot: ${m.imageName}`);
        console.timeEnd(m.imageName);
        workersInWaiting.push(wid);
      }
    });
    worker.send(task);
  } else {
    getWorkerInWaiting(task);
  }

}
function getWorkerInWaiting(task){
  if(workersInWaiting.length){
    workers[workersInWaiting.shift()].send(task);
  } else {
    setTimeout(() => {
      getWorkerInWaiting(task);
    }, 3000)
  }
}